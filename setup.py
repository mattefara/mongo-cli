import requests
import json

from pathlib import Path



# API_KEY = 'k_c4qbv4pg'
API_KEY = 'k_bz4swzhu'
BASE_DIR = Path('dataset/')

def download_top_250():
    data = requests.get(f'https://imdb-api.com/en/API/Top250Movies/{API_KEY}').json()
    with open(BASE_DIR / 'top250.json', 'w') as f:
        json.dump(data, f)
    return data

def downlaod_films(_id):
    return requests.get(f'https://imdb-api.com/en/API/Title/{API_KEY}/{_id}/FullActor,FullCast,Ratings').json()


if __name__ == '__main__':
    # data = download_top_250()
    data = json.load(open(BASE_DIR / 'top250.json', 'r'))

    collected_data = []
    for film in data['items']:
        _id = film['id']
        print(f'Downloading {_id}')
        collected_data.append(downlaod_films(_id))
    
    with open(BASE_DIR / 'films.json', 'w') as f:
        json.dump(collected_data, f)
        

    