import json
import os
import sys
from logging import Logger, DEBUG
from pathlib import Path
from sys import exit
from typing import List, Set, Iterable

from pymongo import MongoClient
from pymongo.database import Database, OperationFailure

_logger = Logger(__name__, level=DEBUG)


# Exceptions
class QuitException(Exception):
    pass


class SelectionException(Exception):
    pass


# Menu
class MenuController:

    def __init__(self, menu, dispatcher):
        self.menu = menu
        self.dispatcher = dispatcher
        self.context = dict()

    def get_context(self, **kwargs):
        return {'__is_context__': True}

    def run_menu(self, db: Database, input_message: str, **context):
        context.update(**self.get_context())
        while True:
            print(format(self.menu))
            s = f"using ({context.get('collection')}) " if context.get('collection') else ''
            user_input = input(input_message + s)
            try:
                if not self.menu[user_input]:
                    print('Wrong input.')
                    continue
                result = self.dispatcher(user_input, db, **context)
                if isinstance(result, dict) and result.get('__is_context__', True):
                    context.update({**self.get_context(), **result})
                elif result:
                    return result
            except QuitException:
                break


class MenuDispatcher:
    def __init__(self, menu, **context):
        self.functions = {}
        for entry in menu:
            self.add(entry.key, self.bind_function(entry.callback), **{**context, **entry.context})
        self.context = context

    def add(self, key, function, **context):
        self.functions[key] = {'function': function, 'context': context}

    @staticmethod
    def bind_function(function_name, **context):
        if function_name:
            return getattr(sys.modules[__name__], function_name)

    def __call__(self, key, *args, **kwargs):
        function_object = self.functions.get(key)
        if not function_object:
            return
        return function_object['function'](*args, **{**kwargs, 'function_context': function_object['context']})


class MenuEntry:
    def __init__(self, key, message, callback=None, **context):
        self.key = key
        self.message = message
        self.callback = callback
        self.context = context or {}

    def __format__(self, format_spec):
        return f'{self.key}: {self.message}.'

    def __call__(self, *args, **kwargs):
        return self.callback(*args, **kwargs)


class Menu:
    def __init__(self, entries: List[MenuEntry]):
        self.entries = {entry.key: entry for entry in entries}
        self.has_quit = False

    def __format__(self, format_spec):
        return '\n'.join([format(entry) for entry in self.entries.values()])

    @classmethod
    def from_json(cls, filename):
        menu_entries = json.load(open(filename))['menu_entries']
        entries = [MenuEntry(**entry) for entry in menu_entries]
        return cls(entries)

    def quit(self, key: str = None):
        if key.lower() in ('q', 'quit', 'exit'):
            raise QuitException("Quitting menu")

    def __getitem__(self, key):
        self.quit(key)

        return self.entries.get(key, False)

    def __iter__(self):
        for entry in self.entries.values():
            yield entry


def run_menu(db: Database, input_message, filename=None, menu=None, **context):
    assert not (filename is None and menu is None)

    current_collection = context.get('collection', None)
    if not current_collection:
        raise SelectionException('No collection selected')

    if not menu:
        menu = Menu.from_json(filename)
    dispatcher = MenuDispatcher(menu)

    controller = MenuController(menu, dispatcher)
    try:
        return controller.run_menu(db, input_message, **context)
    except OperationFailure as e:
        print(f'An error occurred during a db operation: {e}')
    except json.decoder.JSONDecodeError:
        print(f'Error during evaluation on JSON file')


def fallback_input(msg: str, accepted_answers: Iterable[str], norm=None, default=None, loop: bool = True):
    assert default and loop

    if not norm:
        norm = str.lower

    r = input(f'{msg}: ({",".join(accepted_answers)}) ')
    if default and not r:
        return default

    while loop and norm(r) not in accepted_answers:
        r = input(f'{msg}: ({(",").join(accepted_answers)}) ')

    return r


def validate_data(f, message, default=None):
    while True:
        try:
            value = input(message)
            if default is not None and not value:
                return default
            value = f(value)
        except ValueError:
            continue
        else:
            return value


def select_properties(sample: Iterable, single=False):
    # properties = {key for key in sample}
    if single:
        return input(f'Select one property/value from: ')

    return set(input(f'Select one or more properties (separated by a space): ').split(' '))


def set_properties(props: Set, selected_props: Set, is_subset=True):
    if is_subset and not selected_props.issubset(props):
        return {}

    query = {}
    for prop in selected_props:
        value = input(f'Set a value for {prop}: ')
        try:
            value = int(value)
        except ValueError:
            query[prop] = value
        else:
            query[prop] = value

    return query


# Connection
def establish_connection(*args, test_connection=True):
    client = MongoClient(*args)
    if test_connection:
        _logger.debug(client.server_info())
    return client


# Loading data
def load(db: Database, **context):
    folder = context.get('upload_folder')
    collection = context.get('collection')

    file_path = input('Choose a json file: ')
    path = Path(file_path)
    if not path.is_absolute():
        path = folder / path
    if not path.exists():
        print(f'Cannot find file at {path.absolute()}')
        return
    if not path.is_file():
        print(f'{path.absolute()} must be a file')
        return
    if 'json' not in str(path).lower():
        print(f'{path} must be a JSON file')
        return

    with open(path, 'r') as f:
        data = json.load(f)
        if isinstance(data, list):
            db[collection].insert_many(data)
        if isinstance(data, dict):
            db[collection].insert_one(data)

    print('Done loading data')
    return


# Query menus
def insert(db: Database, **context):
    collection = db[context.get('collection')]

    document = create_nested_object()

    if document:
        collection.insert_one(document)
        print('Insert done.')


def update(db: Database, **context):
    collection = db[context.get('collection')]

    q = create_nested_object()
    document = create_nested_object()

    if document:
        collection.update_many(q, {"$set": document})
        print('Update done.')


def query(db: Database, **context):
    collection = db[context.get('collection')]
    menu_folder = context.get('menu_folder')
    result = run_menu(db, f'Select one of this options:', filename=menu_folder / 'menu_query.json', **context)
    print(result)


def delete(db: Database, **context):
    collection = context.get('collection')

    results = find(db, **context)
    if not results:
        print('Nothing to show')
        return

    print(results)

    r = fallback_input('ALL documents matching the results will be eliminated, are you sure? [y/N] ',
                       ('y', 'yes', 'n', 'no'), default='n')
    if r.lower() in ('n', 'no'):
        print('Delete cancelled')
        return
    for r in results:
        db[collection].delete_one({"_id": r['_id']})
    print("Delete done")


# Querying the database        
def find_all(db: Database, **context):
    collection = context.get('collection')
    limit = validate_data(int, 'Set a limit (0 for all, default 50)', 50)
    if limit <= 0:
        return list(db[collection].find({}))

    return list(db[collection].find({}).limit(limit))


def find(db: Database, **context):
    collection = db[context.get('collection')]
    return list(collection.find(create_nested_object()))


def create_nested_object(prop=None, sample=None):
    if sample is None:
        sample = {}
    obj = {}

    if prop:
        print(f'Evaluating prop {prop}')

    props = input('Add property name (separate by space for multiple properties): ').split(' ')

    for p in props:
        r = input(f'Create a nested object at {p}? [y/N] ')
        if r.lower() in ('y', 'yes'):
            obj[p] = create_nested_object(p, sample)
            continue

        r = input('Is an array? [y/N] ')
        if r.lower() in ('y', 'yes'):
            print('Creating array')
            items = []
            done = False
            while not done:
                r = input('Is an object? [y/N] ')
                if r.lower() not in ('y', 'yes'):
                    items.extend(set_properties(set(), {p}, is_subset=False).values())
                else:
                    items.append(create_nested_object(p, sample))
                d = input("Continue? [y/N] ").lower()
                done = d not in ('y', 'yes')    
            obj[p] = items
            continue

        obj.update(set_properties(set(), {p}, is_subset=False))
    return obj


def aggregate(db: Database, **context):
    collection = db[context.get('collection')]

    aggregate_functions = ["$match", "$project", "$group", "$sum", "$unwind", "$sort"]
    result = collection.find_one() or {}

    done = False
    query = []
    while not done:
        query.append(create_nested_object(sample=aggregate_functions))

        print(f'Current pipeline: {query}')

        done = input('Done [y/n]: ').lower()
        while done not in ('y', 'n', 'yes', 'no'):
            print('Error, Done [y,n]')
            done = input('Done [y/n]: ').lower()
        done = done in ('y', 'yes')
    return list(collection.aggregate(query))


def load_query(db: Database, **context):
    query_folder = context.get('query_folder')
    files = os.listdir(query_folder)
    menu = Menu([MenuEntry(str(i), name, callback='run_loaded_query', filename=name) for i, name in enumerate(files)])
    return run_menu(db, 'Select one of these queries', menu=menu, **context)


def run_loaded_query(db, **context):
    collection = context.get('collection')
    query_folder = context.get('query_folder')
    selected_file = context.get('function_context').get('filename')

    with open(query_folder / selected_file) as f:
        data = json.load(f)

    _type = data['type']
    if _type not in ('aggregate', 'find'):
        raise NameError(f'Type {_type} is not supported')

    if _type == 'aggregate':
        return list(db[collection].aggregate(data['query']))
    if _type == 'find':
        return list(db[collection].find(data['query']))


def select_collection(db: Database, collections: List[str] = None, **context):
    if not collections:
        collections = db.list_collection_names()

    collection = input(f'Select one of these collections: {collections} ')
    if collection not in collections:
        print(f'Collection {collection} will be created')

    context.update({'collection': collection})

    return context


# Main
def main():
    CONNECTION_NAME = '172.17.0.1', 27017
    DATABASE_NAME = 'imdb'
    QUERY_FOLDER = Path('queries/')
    UPLOAD_FOLDER = Path('uploads/')
    MENU_FOLDER = Path('menus/')

    print(f'Connecting to {CONNECTION_NAME}')
    print(f'Using database {DATABASE_NAME}')

    client = establish_connection(*CONNECTION_NAME)
    db = client[DATABASE_NAME]
    collections_names = db.list_collection_names()

    current_collection = None
    try:
        current_collection = select_collection(db, collections_names)['collection']
    except SelectionException:
        print('Some data is expected, exiting')
        exit(0)

    run_menu(db, filename=MENU_FOLDER / 'menu_operations.json',
             input_message=f'Select one of these options: ', collection=current_collection, query_folder=QUERY_FOLDER,
             upload_folder=UPLOAD_FOLDER, menu_folder=MENU_FOLDER)


if __name__ == '__main__':
    main()
